
if [[ ! -z "${__MY_RC__}" ]]; then
  echo "load_bak ${__MY_RC__}"
  sleep 1
  export __MY_RC__=1
  if [ -f "$HOME/.bashrc.bak" ]; then
    . $HOME/.bashrc.bak
  fi

  if [ -f "$HOME/.bash_profile.bak" ]; then
    . $HOME/.bash_profile.bak
  fi
fi

if [[ "" == "$INSTALL_DIR" ]] ; then
  SOURCE="${BASH_SOURCE[0]}"
  while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
    DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
    SOURCE="$(readlink "$SOURCE")"
    [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
  done
  export INSTALL_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
fi


if [ -f $INSTALL_DIR/autoupdate.sh ]; then
  . $INSTALL_DIR/autoupdate.sh
  rm -f $INSTALL_DIR/autoupdate.sh
else
  FORCE_UPDATE=1
fi


for A in $INSTALL_DIR/*.env.sh ; do
	. $A
done

