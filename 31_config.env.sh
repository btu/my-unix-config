
##################################################
# Brew.                                          #
##################################################

if [[ "$OS" == "Darwin" ]]; then
  if [ $USE_BREW != 0 ]; then
    if [[ "$OS" == "Darwin" && ! `which brew` ]]; then 
      echo "Installing brew";
    elif [ "$FORCE_UPDATE" != "" ]; then
      echo "Brew is outdated, update with 'brew update && brew upgrade'"
      # brew update
      # brew upgrade
    fi
    source `brew --repository`/Library/Contributions/brew_bash_completion.sh
  fi
fi

##################################################
# Tmux Conf.                                     #
##################################################

if [ $USE_TMUX ]; then
  if [ ! -f $HOME/.tmux.conf ] ; then
  	ln -s $INSTALL_DIR/tmux.conf $HOME/.tmux.conf
  fi

  ## make airline
  ## https://github.com/edkolev/tmuxline.vim
  #vim -c 'Tmuxline [theme] [preset] | TmuxlineSnapshot ~/.tmux-snapshot.conf | wq'
#else
  # deletion is not required
  # if [ -f $HOME/.tmux.conf ] ; then
  #   rm $HOME/.tmux.conf
  # fi
fi

##################################################
# LS Colors                                      #
##################################################

export CLICOLOR=1
if [[ $CONSOLE_BG == "clear" ]]; then
  #Clear
  export LSCOLORS=ExFxCxDxBxegedabagacad
else
  #Dark
  export LSCOLORS=gxBxhxDxfxhxhxhxhxcxcx
fi

##################################################
# Font.                                          #
##################################################
if [[ "$OS" == "Darwin" ]]; then
  FONT_DIR="$HOME/Library/Fonts"
  MD5_EX="md5 -q "
  if [[ $USE_FONT == 0 ]]; then USE_FONT=3; fi #patched monaco for mac
elif [[ "$OS" == "Linux" ]]; then
  FONT_DIR="$HOME/.fonts"
  MD5_EX="md5sum "
fi
if [[ "$FONT_DIR" != "" ]]; then 
  if [[ ! -f $FONT_DIR/term_font.otf || "$FORCE_UPDATE" != "" ]] ; then
    echo "Updating font"
    case $USE_FONT in 
      0) # no font
        rm $FONT_DIR/term_font.otf
        rm $INSTALL_DIR/term_font.otf
        ;;
      1)
        #curl -fsSLo $INSTALL_DIR/term_font.otf https://raw.github.com/Lokaltog/powerline-fonts/master/SourceCodePro/Sauce%20Code%20Powerline%20Regular.otf 2> /dev/null
        curl -fsSLo $INSTALL_DIR/term_font.otf https://github.com/powerline/fonts/raw/master/SourceCodePro/Sauce%20Code%20Powerline%20Regular.otf 2> /dev/null
        ;;
      2)
        curl -fsSLo $INSTALL_DIR/term_font.otf https://github.com/powerline/fonts/raw/master/SourceCodePro/Sauce%20Code%20Powerline%20Medium.otf 2> /dev/null
        ;;
      3)
        curl -fsSLo $INSTALL_DIR/term_font.otf https://raw.github.com/mneorr/powerline-fonts/bfcb152306902c09b62be6e4a5eec7763e46d62d/Monaco/Monaco%20for%20Powerline.otf 2> /dev/null
        ;;
    esac
    if [ -f $INSTALL_DIR/term_font.otf ]; then
      mkdir -p $FONT_DIR
      run=1

      if [[ "$MD5_EX" == "" || "`$MD5_EX $INSTALL_DIR/term_font.otf`" != "`$MD5_EX $FONT_DIR/term_font.otf`" ]]; then
        cp $INSTALL_DIR/term_font.otf $FONT_DIR
        echo "Font has been installed, please set '* for Powerline' as a font for your terminal."
      fi
    fi
  fi
fi



##################################################
#  Awesome vimrc
# https://github.com/amix/vimrc
##################################################

alias vi="vim"

if [[ `which git 2> /dev/null` ]] ; then 
    if [ $USE_VIM_BASIC != 0 ] ; then
        if [[ ! -d ~/.vim_runtime ]]; then
            echo "Installing vimrc"
            git clone git://github.com/amix/vimrc.git ~/.vim_runtime
        fi

        if [[ "$FORCE_UPDATE" != "" ]] ; then
            echo "Updating vimrc"
            cd ~/.vim_runtime
            git pull --rebase > /dev/null &
            cd -
            if [[ `which ctags` ]]; then
                if [ $USE_VIM_EXTENDED != 0 ] ; then
                    sh ~/.vim_runtime/install_awesome_vimrc.sh
                    if [ -f $FONT_DIR/term_font.otf ]; then
                        echo "let g:airline_powerline_fonts = 1" > $HOME/.vim_runtime/my_configs.vim
                    fi
                else
                    sh ~/.vim_runtime/install_basic_vimrc.sh
                fi
            else
                echo "ctags not found, can't setup vim_awesome"
            fi
        fi
    fi
fi

##################################################
# GIT.	                                         #
##################################################

if [ $USE_GIT != 0 ] ; then

    if [ ! -f $INSTALL_DIR/git-completion.bash ] ; then
        curl -fsSLo $INSTALL_DIR/git-completion.bash https://raw.github.com/git/git/master/contrib/completion/git-completion.bash 2> /dev/null
    fi

    if [ ! -f $INSTALL_DIR/git-prompt.sh ] ; then
        curl -fsSLo $INSTALL_DIR/git-prompt.sh https://raw.github.com/git/git/master/contrib/completion/git-prompt.sh 2> /dev/null
    fi

  #TODO define git template by hook cf http://git-scm.com/book/ch7-3.html
 

  # windows
  #git config --global core.autocrlf true
  #else
  #git config --global core.autocrlf input
  git config --global color.ui true
  GIT_PS1_SHOWCOLORHINTS=1
  GIT_PS1_SHOWDIRTYSTATE=1
  GIT_PS1_SHOWUPSTREAM="auto"
  GIT_PS1_DESCRIBE_STYLE="describe"
  source $INSTALL_DIR/git-prompt.sh
  source $INSTALL_DIR/git-completion.bash

  if [ $USE_GIT_ALIAS != 0 ] ; then
    alias gs='git status'
    alias gl='git pull'
    alias gp='git push'
    alias gd='git diff'
    alias gau='git add --update'
    alias gc='git commit -v'
    alias gca='git commit -v -a'
    alias gb='git branch'
    alias gba='git branch -a'
    alias gco='git checkout'
    alias gcob='git checkout -b'
    alias gcot='git checkout -t'
    alias gcotb='git checkout --track -b'
    alias glog='git log'
    alias glogp='git log --pretty=format:"%h %s" --graph'
  fi
else
  function __git_ps1(){
    PS1=$1$2
  }
fi


##################################################
# PROMPT.                                        #
##################################################

if [ "$CONSOLE_BG" == "dark" ]; then
  col_usr='\[\e[1;36m\]' # Cyan
  col_dir='\[\e[35;1m\]' # purple
  col_host='\[\e[0;32m\]' # Green
  col_prt_ok='\[\e[0;32m\]' # Green
  col_prt_ko='\[\e[0;31m\]' # Green
  col_prt=$col_ptr_ok
  col_rs='\[\e[0m\]'  # Reset
else
  col_usr='\[\e[34m\]' # Blue?
  col_dir='\[\e[35;1m\]' # purple
  col_prt_ok='\[\e[0;32m\]' # Green
  col_prt_ko='\[\e[0;31m\]' # red
  col_prt=$col_ptr_ok
  col_rs='\[\e[0m\]'  # Reset
fi

case $PROMPT_LEVEL in
  0 )
    PS1=' \$ '
    PROMPT_COMMAND=''
    ;;
  1 ) 
    PROMPT_COMMAND='__git_ps1 "" "$col_prt $RET\$ $col_rs"'
    ;;
  2 )
    PROMPT_COMMAND='__git_ps1 "$col_dir\W$col_rs" "$col_prt \$ $col_rs"'
    ;;
  3 )
    PROMPT_COMMAND='__git_ps1 "$col_dir\W$col_rs" "$col_prt (\j_$?)\$ $col_rs"'
    ;;
  4 )
    PROMPT_COMMAND='__git_ps1 "\[\033]0;\w\007\]$col_usr\u$col_rs$col_host@\h$col_dir\w$col_rs" "$col_prt \$ $col_rs"'
    ;;
esac

PROMPT_COMMAND='RET=$?;history -a && history -n; if [[ $RET -eq 0 ]]; then col_prt="\[\e[0;32m\]" ;else col_prt="\[\e[0;31m\]" ; fi;'"$PROMPT_COMMAND"


#Full commandline
#PROMPT_COMMAND='__git_ps1 "\[\033]0;\w\007\]\[\e[35;1m\]\u\[\e[0m\]\[\e[32m\]@\h\[\e[34m\]\w" "\[\e[33m\] \$ \[\e[0m\]"'
#simple one
#PROMPT_COMMAND='__git_ps1 "\[\e[35;1m\]\u\[\e[0m\]" "\[\e[33m\] \$ \[\e[0m\]"'
#gitonly
#PROMPT_COMMAND='__git_ps1 "" "\[$txtgrn\] \$ \[$txtrst\]"'



# Powerline is too heavy and ugly font are cool though
#source /usr/local/lib/python2.7/site-packages/powerline/bindings/bash/powerline.sh


