################################################################################
###
### Default Config
###
###
### this is the default config, please do NOT overwrite this file.
### instead create a 01_config.env file to persist your personal configurations
###
################################################################################

UPDATE_CYCLE_DAY=7      #defined how often to check for update in days

################################################################################
# Tools
################################################################################

USE_BREW=1                    # Autocomplete and update
USE_VIM_BASIC=0
USE_VIM_EXTENDED=0
USE_GIT=1
USE_GIT_ALIAS=1               #defines some git alias (USE_GIT should be true)
USE_TMUX=0

################################################################################
# Font
################################################################################

USE_FONT=0                  # auto
#USE_FONT=1                 # source code pro Regular, 2 => Medium
#USE_FONT=2                 # source code pro Medium
#USE_FONT=3                 # Monaco for mac

################################################################################
# Checks
################################################################################

USE_JAVA=0
USE_ANDROID=0
USE_ANDROID_NDK=0

################################################################################
# Bash
################################################################################

PROMPT_LEVEL=4                         # try 1~4 for different flavors, from more simple to more buzy
CONSOLE_BG="dark"                      # Console theme : ["dark","clear"]
USE_CD_EXT=1                           # `cd` extension. try cd -- # try cd -{0~9} 


################################################################################
# TODO, not implemented
################################################################################

VARIABLES_TO_DROPBOX= #not implemented
#VARIABLES_TO_DROPBOX=~/Dropbox/bash_var/ # this will save your local config (01_*.env) to dropbox. Note that 01_local.env will not be saved