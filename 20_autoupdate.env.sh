

# if not exists, create a autoupdate.sh file that will check for a given time and execute only if reach

if [ ! -f $INSTALL_DIR/autoupdate.sh ]; then

  time=`date +"%s"`
  let tgt_time="$time+(86400 * ${UPDATE_CYCLE_DAY})"

  #cat file
cat <<EOF > $INSTALL_DIR/autoupdate.sh
#start autoupdate
tgt_time=$tgt_time
time=\`date +"%s"\`
if (( \$time > \$tgt_time )) ; then
  echo "It's time to update"
  cd $INSTALL_DIR
  git pull --rebase
  cd -
  FORCE_UPDATE=1
fi
#end autoupdate
EOF
  #end
fi
