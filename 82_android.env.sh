

##################################################
# Android.                                       #
##################################################

if [ $USE_ANDROID != 0 ]; then

  if [[ "" == "$ANDROID_SDK_HOME" ||  ! -d $ANDROID_SDK_HOME ]] ; then
    echo "[Android] ANDROID_SDK_HOME is not set or does not exist; save it with 'savewd' or 'brew install android-sdk'"
  else
    export PATH=$PATH:$ANDROID_SDK_HOME/tools
    export PATH=$PATH:$ANDROID_SDK_HOME/platform-tools
  fi
  

  if [ $USE_ANDROID_NDK != 0 ]; then
    if [[ "" == "$ANDROID_NDK_HOME" || ! -d $ANDROID_NDK_HOME ]] ; then
      echo "[Android] ANDROID_NDK_HOME is not set or does not exist; save it with 'savewd' or 'brew install android-ndk'"
    else
      export PATH=$PATH:$ANDROID_NDK_HOME/
    fi
  fi

  if [[ ! `which ant` ]]; then
    echo "[Android] ant is not installed"
  fi

fi