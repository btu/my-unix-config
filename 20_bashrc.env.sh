
# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[ -z "$PS1" ] && return


##################################################
##################################################
##################################################

if [[ `which vim` ]]; then
    export EDITOR=vim
elif [[ `which vi` ]]; then
    export EDITOR=vi
fi
export BLOCKSIZE=K              # set blocksize size
export BROWSER='chrome'          # set default browser

###############
### History
###############
# don't put duplicate lines in the history. See bash(1) for more options
# ... or force ignoredups and ignorespace
export HISTCONTROL=ignoredups:ignorespace
# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
export HISTSIZE=5000
export HISTFILESIZE=10000
export HISTTIMEFORMAT='%Y-%m-%d_%H:%M:%S_%a  '  # makes history display in YYYY-MM-DD_HH:MM:SS_3CharWeekdaySpaceSpace format
# append to the history file, don't overwrite it
shopt -s histappend

export HOSTFILE=$HOME/.hosts            # put list of remote hosts in ~/.hosts ...

shopt -s cdable_vars                # set the bash option so that no '$' is required (disallow write access to terminal)
shopt -s cdspell                # this will correct minor spelling errors in a cd command
shopt -s checkhash
# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize               # update windows size on command
shopt -s cmdhist                    # save multi-line commands in history as single line
shopt -s histappend histreedit histverify

shopt -s extglob                # necessary for bash completion (programmable completion)

OS=$(uname)		# for resolving pesky os differing switches


##################################################
##################################################
##################################################


# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac


# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi



##################################################
# More PROMPT_COMMANDS				 #
##################################################


###### Shows memory, load average, and history
# PROMPT_COMMAND='history -a;echo -en "\033[m\033[38;5;2m"$(( `sed -nu "s/MemFree:[\t ]\+\([0-9]\+\) kB/\1/p" /proc/meminfo`/1024))"\033[38;5;22m/"$((`sed -nu "s/MemTotal:[\t ]\+\([0-9]\+\) kB/\1/Ip" /proc/meminfo`/1024 ))MB"\t\033[m\033[38;5;55m$(< /proc/loadavg)\033[m"'



###### Shows the return value of the last executed command (using smileys as to whether it was successful or not)
# PROMPT_COMMAND='RET=$?; if [[ $RET -eq 0 ]]; then echo -ne "\033[0;32m$RET\033[0m ;)"; else echo -ne "\033[0;31m$RET\033[0m ;("; fi; echo -n " "'


##################################################
# Various options to make $HOME comfy		 #
##################################################

if [ ! -d "${HOME}/bin" ]; then
    mkdir ${HOME}/bin
    chmod 700 ${HOME}/bin
    echo "${HOME}/bin was missing.  I created it for you."
fi
export PATH=$PATH:${HOME}/bin

if [ ! -d "${HOME}/Documents" ]; then
    if ! [  -d "${HOME}/data" ]; then
        mkdir ${HOME}/data
        chmod 700 ${HOME}/data
        echo "${HOME}/data was missing.  I created it for you."
    fi
fi

if [ ! -d "${HOME}/tmp" ]; then
    mkdir ${HOME}/tmp
    chmod 700 ${HOME}/tmp
    echo "${HOME}/tmp was missing.  I created it for you."
fi


##################################################
# Stop Flash from tracking everything you do.	 #
##################################################

###### Brute force way to block all LSO cookies on Linux system with non-free Flash browser plugin
for A in ~/.adobe ~/.macromedia ; do ( [ -d $A ] && rm -rf $A ; ln -s -f /dev/null $A ) ; done



##################################################
# Basic aliases               #
##################################################


# enable color support of ls and also add handy aliases
if [[ "$OS" = "Darwin" ]]; then
    alias ls='ls -GhpF'
elif [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls -h --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi


# Directory navigation aliases
alias cd..='cd ..'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'

# Functions
extract_func () {
	if [ -f $1 ] ; then
		case $1 in
			*.tar.bz2)	tar xjf $1		;;
			*.tar.gz)	tar xzf $1		;;
			*.bz2)		bunzip2 $1		;;
			*.rar)		rar x $1		;;
			*.gz)		gunzip $1		;;
			*.tar)		tar xf $1		;;
			*.tbz2)		tar xjf $1		;;
			*.tgz)		tar xzf $1		;;
			*.zip)		unzip $1		;;
			*.Z)		uncompress $1	;;
			*)			echo "'$1' cannot be extracted via extract()" ;;
		esac
	else
		echo "'$1' is not a valid file"
	fi
}
alias extract=extract_func

function h() {
    if [ -z "$1" ]
    then
        history
    else
        history | grep "$@"
    fi
}

#function askvar(){}

function savewd() {
    ## save the working directory, used for setting path for instance
    varname=$1
    todir=`pwd`
    if [ -z "$varname" ] ; then
        #read from input
        echo "You will save the current folder : $todir"
        echo "Please enter a variable name : "
        read varname
        
    fi
    echo "Saving '$varname=$todir'"
    echo "export $varname=$todir # $USER on `date`" >> $INSTALL_DIR/11_local.env.sh
    . $INSTALL_DIR/11_local.env.sh
}

function save() {
    # save ID_CASPER 107c5134-35f8-40fb-afdd-ce7fd3235ad3
    if [ -z "$2" ] ; then
        echo "persistantly save an environment variable."
        echo "/!\\ expects 2 arguments. ex: 'save {varname} {value}'"
    else
        target_fn=$3
        if [[ "" == "$target_fn" ]] ; then
            target_fn="vars"
        fi
        if [[ "config" == "$target_fn" ]]; then
          echo "$1=$2 # $USER on `date`" >> $INSTALL_DIR/11_${target_fn}.env.sh
        else
          echo "export $1=$2 # $USER on `date`" >> $INSTALL_DIR/11_${target_fn}.env.sh
        fi
        . $INSTALL_DIR/11_${target_fn}.env.sh
    fi
}
function savesettings() {
  if [[ "$1" == "1" || "$1" == "reload" ]]; then
    rm $INSTALL_DIR/autoupdate.sh
    . $HOME/.bash_profile #$INSTALL_DIR/load_profile.sh
  elif [ -z "$2" ] ; then
    echo "Current settings : "
    grep -v '^#' $INSTALL_DIR/00_default_config.env.sh  | grep -v '^$' | awk -F= 'NR > 1 { print "printf \"\t%-50s %s\\n\" \"" $1 "\" \"$" $1 "\"" }' > tmp.sh && . tmp.sh && rm tmp.sh
    echo "To set a setting, use 'savesettings SETTING_NAME value'"
  else
    save $1 $2 "config"
    rm $INSTALL_DIR/autoupdate.sh
    . $HOME/.bash_profile #$INSTALL_DIR/load_profile.sh
  fi
}

# b) function cd_func
# This function defines a 'cd' replacement function capable of keeping,
# displaying and accessing history of visited directories, up to 10 entries.
# To use it, uncomment it, source this file and try 'cd --'.
# acd_func 1.0.5, 10-nov-2004
# Petar Marinov, http:/geocities.com/h2428, this is public domain
 cd_func ()
 {
   local x2 the_new_dir adir index
   local -i cnt

   if [[ $1 ==  "--" ]]; then
     dirs -v
     return 0
  fi

   the_new_dir=$1
   [[ -z $1 ]] && the_new_dir=$HOME

   if [[ ${the_new_dir:0:1} == '-' ]]; then
     #
     # Extract dir N from dirs
     index=${the_new_dir:1}
     [[ -z $index ]] && index=1
     adir=$(dirs +$index)
     [[ -z $adir ]] && return 1
     the_new_dir=$adir
   fi

   #
   # '~' has to be substituted by ${HOME}
   [[ ${the_new_dir:0:1} == '~' ]] && the_new_dir="${HOME}${the_new_dir:1}"

   #
   # Now change to the new dir and add to the top of the stack
   pushd "${the_new_dir}" > /dev/null
   [[ $? -ne 0 ]] && return 1
   the_new_dir=$(pwd)

   #
   # Trim down everything beyond 11th entry
   popd -n +11 2>/dev/null 1>/dev/null

   #
   # Remove any other occurence of this dir, skipping the top of the stack
   for ((cnt=1; cnt <= 10; cnt++)); do
     x2=$(dirs +${cnt} 2>/dev/null)
     [[ $? -ne 0 ]] && return 0
     [[ ${x2:0:1} == '~' ]] && x2="${HOME}${x2:1}"
     if [[ "${x2}" == "${the_new_dir}" ]]; then
       popd -n +$cnt 2>/dev/null 1>/dev/null
       cnt=cnt-1
     fi
   done

   return 0
 }

 alias cd=cd_func

