

##################################################
# Java.                                          #
##################################################

if [ $USE_JAVA != 0 ]; then
	if [[ "" == "$JAVA_HOME" ||  ! -d $JAVA_HOME ]] ; then
		if [[ $OS == "Darwin" ]] ; then
			if [ ! -d `/usr/libexec/java_home` ] ; then
				echo "'/usr/libexec/java_home' could not find java. Please install java."
			else 
				export JAVA_HOME=`/usr/libexec/java_home`
			fi
		else
			echo "JAVA_HOME was not found! You can set it by going to proper directory and type 'savewd JAVA_HOME' "
		fi
	fi
	if [[ "" != "$JAVA_HOME" ||  -d $JAVA_HOME ]] ; then
		export PATH=$JAVA_HOME/bin:$PATH
	fi
fi
