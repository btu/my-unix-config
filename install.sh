INSTALL_DIR=.myrc

if [[ `which git` ]]; then

  cd $HOME
  git clone https://bitbucket.org/btu/my-unix-config.git $INSTALL_DIR
  cd -

  if [[ -f $HOME/.bashrc && ! -h $HOME/.bashrc ]]; then
    mv $HOME/.bashrc $HOME/.bashrc.bak
  fi
  if [ -h $HOME/.bashrc ] ; then
      rm $HOME/.bashrc
  fi
  ln -s $HOME/$INSTALL_DIR/load_profile.sh $HOME/.bashrc

  if [[ -f $HOME/.bash_profile && ! -h $HOME/.bash_profile ]]; then
    mv $HOME/.bash_profile $HOME/.bash_profile.bak
  fi
  ln -s $HOME/$INSTALL_DIR/load_profile.sh $HOME/.bash_profile
  
  if [[ -f $HOME/.inputrc && ! -h $HOME/.inputrc ]]; then
    mv $HOME/.inputrc  $HOME/.inputrc.bak
  fi
  ln -s $HOME/$INSTALL_DIR/inputrc $HOME/.inputrc 
  #echo "INSTALL_DIR=$INSTALL_DIR" >> $HOME/$INSTALL_DIR/11_local.env.sh

  echo "Install successful. all new terminal will apply changes"

else

  echo "Installation requires git, please install it first"

fi
